const form = document.querySelector('form');

export default (onStart, onSuccess, onError, onEnd) => {
  const lineInput = document.getElementById('line');

  lineInput.addEventListener('invalid', (e) =>
    e.target.setCustomValidity('Podaj jakiej linii szukasz'));

  lineInput.addEventListener('input', (e) =>
    e.target.setCustomValidity(''));

  form.addEventListener('submit', (e) => {
    const line = lineInput.value;
    const type = document.getElementById('type').checked;

    e.preventDefault();

    onStart();

    fetch(`https://m-bustrams.herokuapp.com/q?type=${type ? 2 : 1}&line=${line}`)
      .then((res) => res.json())
      .then(onSuccess)
      .catch(onError)
      .finally(onEnd);
  });
}
