import L from 'leaflet';
import form from './form';

import './index.css';

const map = new L.map('map', {attributionControl: false}).setView([52.232222, 21.008333], 11);
const userIcon = new L.divIcon({className: 'user-icon'});
const markers = {
  user: new L.marker([], {icon: userIcon}),
  vehicles: []
};
const watchID = navigator.geolocation.watchPosition((position) => {
  const { latitude, longitude } = position.coords;
  const userPosition = L.latLng(latitude, longitude);

  markers
    .user
    .setLatLng(userPosition)
    .addTo(map);

  // map.panTo(markers.user.getLatLng());
});

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibW1hayIsImEiOiJjam5yOGZ2cmUwNTg1M3dwamgwcDNmaDV2In0.6mXnTCzzWvbrUPPx71uP5Q', {
  maxZoom: 18,
  id: 'mapbox.streets'
}).addTo(map);

form(
  () => {
    const submitCta = document.getElementById('submit');
    submitCta.setAttribute('disabled', 'disabled');
    document.body.style.opacity = '0.5';
  },
  (json) => {
    const submitCta = document.getElementById('submit');
    submitCta.removeAttribute('disabled');
    markers.vehicles.forEach(removeMarker)
    markers.vehicles = json.result.map((vehicle) => new L.marker([vehicle.Lat, vehicle.Lon]).addTo(map));
    document.body.style.opacity = '1';
  },
  (err) => console.log(err),
  () => document.body.style.opacity = '1'
);



/* utils */

function removeMarker(marker) {
  marker.remove();
}